package ru.iteco.taskmanager.api;

import java.util.Map;

import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.command.AbstractCommand;

@Component
public interface ITerminalService {

    void put(final String command, final AbstractCommand abstractCommand);

    AbstractCommand get(final String command);

    Map<String, AbstractCommand> getCommands();
}
