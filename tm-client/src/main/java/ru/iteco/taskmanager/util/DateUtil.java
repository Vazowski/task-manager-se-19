package ru.iteco.taskmanager.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DateUtil {

    private static DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);

    public static String getDate(@Nullable final String date) {
	try {
	    return df.parse(date).toString();
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return null;
    }
}
