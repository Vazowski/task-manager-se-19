package ru.iteco.taskmanager.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ITerminalService;
import ru.iteco.taskmanager.command.AbstractCommand;

@Getter
@Setter
@Component
public class TerminalService extends AbstractService implements ITerminalService {

    @NotNull
    private Map<String, AbstractCommand> commands;

    public TerminalService() {
	commands = new LinkedHashMap<String, AbstractCommand>();
    }

    public void put(@NotNull final String command, @NotNull final AbstractCommand abstractCommand) {
	commands.put(command, abstractCommand);
    }

    @Nullable
    public AbstractCommand get(@NotNull final String command) {
	return commands.get(command);
    }
}
