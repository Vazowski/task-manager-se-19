package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Component
public class UserShowInfoCommand extends AbstractCommand {

    @Autowired
    private IUserEndpoint userEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Override
    public String command() {
	return "user-show";
    }

    @Override
    public String description() {
	return "  -  show user info";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;
	System.out.println("First name: " + userDTO.getFirstName());
	System.out.println("Last name: " + userDTO.getLastName());
	System.out.println("Middle name: " + userDTO.getMiddleName());
	System.out.println("Email: " + userDTO.getEmail());
	System.out.println("Phone: " + userDTO.getPhone());
	System.out.println("Role: " + userDTO.getRoleType());
    }
}
