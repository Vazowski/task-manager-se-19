package ru.iteco.taskmanager.command.serialize.save;

import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Component
public class DataToXmlJacksonCommand extends AbstractCommand {

    @Autowired
    private IDomainEndpoint domainEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Override
    public String command() {
	return "jackson-to-xml";
    }

    @Override
    public String description() {
	return "  -  save data to xml via jackson";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;

	if (domainEndpoint.jacksonSaveXml(sessionDTO) != null) {
	    sessionService.setSession(null);
	    System.out.println("Done");
	}
    }

}
