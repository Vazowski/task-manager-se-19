package ru.iteco.taskmanager.command.task.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Component
public class TaskFindAllByNamePartCommand extends AbstractCommand {

    @Autowired
    private IUserEndpoint userEndpoint;
    @Autowired
    private IProjectEndpoint projectEndpoint;
    @Autowired
    private ITaskEndpoint taskEndpoint;
    @Autowired
    private ISessionService sessionService;

    @NotNull
    private String partOfName;

    @Override
    public @NotNull String command() {
        return "task-find-all-name-part";
    }

    @Override
    public @NotNull String description() {
        return "  -  find all task by part of name";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        System.out.print("Name of project: ");
        @Nullable final String inputProjectName = scanner.nextLine();
        @Nullable final Project tempProject = ProjectDTOConvertUtil
                .DTOToProject(projectEndpoint.findProjectByName(sessionDTO, inputProjectName));
        if (tempProject == null) {
            System.out.println("Project doesn't exist");
            return;
        }

        System.out.print("Part of name of task: ");
        @NotNull final String partOfName = scanner.nextLine();
        @Nullable final List<Task> taskList = TaskDTOConvertUtil
                .DTOsToTasks(taskEndpoint.findAllTaskByPartOfName(sessionDTO, partOfName));

        if (taskList == null) {
            System.out.println("No task exist");
            return;
        }

        for (int i = 0, j = 1; i < taskList.size(); i++) {
            System.out.println("[Task " + (j++) + "]");
            System.out.println("ID: " + taskList.get(i).getId());
            System.out.println("OwnerID: " + taskList.get(i).getOwnerId());
            System.out.println("ProjectID: " + taskList.get(i).getProjectId());
            System.out.println("Name: " + taskList.get(i).getName());
            System.out.println("Description: " + taskList.get(i).getDescription());
            System.out.println("DateCreated: " + taskList.get(i).getDateCreated());
            System.out.println("DateBegin: " + taskList.get(i).getDateBegin());
            System.out.println("DateEnd: " + taskList.get(i).getDateEnd());
            System.out.println("Status: " + taskList.get(i).getReadinessStatus().toString());
        }
    }
}
