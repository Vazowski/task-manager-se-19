package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Component
public class UserLoginCommand extends AbstractCommand {

    @Autowired
    private ISessionService sessionService;
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @Override
    public String command() {
	return "login";
    }

    @Override
    public String description() {
	return "  -  login user";
    }

    @Override
    public void execute() throws Exception {
	while (true) {
	    System.out.print("Login: ");
	    @NotNull
	    final String login = scanner.nextLine();
	    System.out.print("Password: ");
	    @NotNull
	    final String password = scanner.nextLine();

	    @Nullable
	    final SessionDTO sessionDTO = sessionEndpoint.signSession(login, password);
	    if (sessionDTO == null) {
		System.out.println("Username or password was incorrect. Try again");
		continue;
	    }
	    //sessionDTO.setId(UUID.randomUUID().toString());
	    sessionService.setSession(SessionDTOConvertUtil.DTOToSession(sessionDTO));
	    sessionEndpoint.mergeSession(sessionDTO);
	    System.out.println("Done");
	    break;
	}
    }
}
