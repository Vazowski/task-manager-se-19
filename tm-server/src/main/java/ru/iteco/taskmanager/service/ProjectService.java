package ru.iteco.taskmanager.service;

import java.util.List;

import javax.persistence.NoResultException;
import javax.validation.constraints.Null;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;

@Getter
@Setter
@Service
@Transactional
@NoArgsConstructor
public class ProjectService extends AbstractService implements IProjectService {

	@Autowired
	private ProjectRepository projectRepository;

    public void save(@NotNull final Project project) {
    	projectRepository.save(project);
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        try {
            return projectRepository.getOne(id);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
        try {
            return projectRepository.findByName(name);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Nullable
    public List<Project> findAll() {
    	return projectRepository.findAll();
    }

    @Nullable
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
    	return projectRepository.findAllByOwnerId(ownerId);
    }

    @Nullable
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
    	return projectRepository.findAllByPartOfName(partOfName);
    }

    @Nullable
    public List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription) {
    	return projectRepository.findAllByPartOfDescription(partOfDescription);
    }

    public void remove(@NotNull final String id) {
    	@Nullable
        final Project tempProject = findById(id);
    	if (tempProject == null) return;
    	projectRepository.delete(tempProject);
    }

    public void removeAll() {
        projectRepository.deleteAll();
	}
}
