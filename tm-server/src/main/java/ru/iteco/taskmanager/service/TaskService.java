package ru.iteco.taskmanager.service;

import java.util.List;

import javax.persistence.NoResultException;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;

@Getter
@Setter
@Service
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    public void save(@NotNull final Task task) {
    	taskRepository.save(task);
    }

    public Task findById(@NotNull final String id) {
        try {
            return taskRepository.getOne(id);
        } catch (NoResultException e) {
            return null;
        }
    }

    public Task findByProjectId(@NotNull final String projectId) {
        try {
            return taskRepository.findByProjectId(projectId);
        } catch (NoResultException e) {
            return null;
        }
    }

    public Task findByName(@NotNull final String name) {
        try {
            return taskRepository.findByName(name);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Task> findAll() {
    	return taskRepository.findAll();
    }

    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
    	return taskRepository.findAllByOwnerId(ownerId);
    }

    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
    	return taskRepository.findAllByPartOfName(partOfName);
    }

    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
    	return taskRepository.findAllByPartOfDescription(partOfDescription);
    }

    public void remove(@NotNull final String id) {
    	taskRepository.delete(findById(id));
    }

    public void removeAll() {
        taskRepository.deleteAll();
    }
}
