package ru.iteco.taskmanager.repository;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.taskmanager.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.name=:name")
    Task findByName(@Param("name") @NotNull final String name);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.project.id=:projectId")
    Task findByProjectId(@Param("projectId") @NotNull final String projectId);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.user.id=:ownerId")
    List<Task> findAllByOwnerId(@Param("ownerId") @NotNull final String ownerId);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.name LIKE %:partOfName%")
    List<Task> findAllByPartOfName(@Param("partOfName") @NotNull final String partOfName);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.description LIKE %:partOfDescription%")
    List<Task> findAllByPartOfDescription(@Param("partOfDescription") @NotNull final String partOfDescription);
}
