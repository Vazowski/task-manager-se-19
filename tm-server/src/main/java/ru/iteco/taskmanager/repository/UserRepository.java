package ru.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.taskmanager.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    @Query(value = "SELECT a FROM User a WHERE a.login=:login")
    User findByLogin(@Param("login") @NotNull final String login);
}