package ru.iteco.taskmanager.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "domain")
public class DomainDTO {

    @Nullable
    @XmlElement(name = "project")
    private List<ProjectDTO> projectList;

    @Nullable
    @XmlElement(name = "task")
    private List<TaskDTO> taskList;

    @Nullable
    @XmlElement(name = "user")
    private List<UserDTO> userList;

    public DomainDTO(@Nullable List<UserDTO> userList, @Nullable List<ProjectDTO> projectList, @Nullable List<TaskDTO> taskList) {
	this.userList = userList;
	this.projectList = projectList;
	this.taskList = taskList;
    }
}
