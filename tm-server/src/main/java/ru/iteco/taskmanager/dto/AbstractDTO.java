package ru.iteco.taskmanager.dto;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractDTO {

    @NotNull
    protected String id = UUID.randomUUID().toString();
}
