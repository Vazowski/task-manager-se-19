package ru.iteco.taskmanager.api.service;

import java.util.List;

import ru.iteco.taskmanager.entity.User;

public interface IUserService {

    void save(final User user);

    User findById(final String id);

    User findByLogin(final String login);

    List<User> findAll();
}