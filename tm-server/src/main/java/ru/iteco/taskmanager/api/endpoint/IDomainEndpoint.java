package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.dto.DomainDTO;
import ru.iteco.taskmanager.dto.SessionDTO;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    DomainDTO saveData(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO loadData(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jaxbSaveXml(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jaxbLoadXml(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jaxbSaveJson(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jaxbLoadJson(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jacksonSaveXml(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jacksonLoadXml(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jacksonSaveJson(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    DomainDTO jacksonLoadJson(@WebParam(name = "session") final SessionDTO sessionDTO) throws Exception;
}
